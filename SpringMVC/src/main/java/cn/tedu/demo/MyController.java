package cn.tedu.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping("/my01")
@Controller   //注解：代码简洁，与xml映射效果一致，每个注解，都有自己用途
public class MyController {

    @RequestMapping("/test01.action")
    public ModelAndView test01(){
        ModelAndView mav=new ModelAndView();
        //绑定数据---模型
        mav.addObject("k1","Spring");
        mav.addObject("k2","MVC");
        //调取页面--视图  ---必须存在   test01.jsp(一会创建)
        mav.setViewName("test01");
        return mav;
    }
    @RequestMapping("/test02.action")
    public ModelAndView test02(){
        ModelAndView mav=new ModelAndView();
        //绑定数据---模型
        mav.addObject("k1","Spring");
        mav.addObject("k2","MVC");
        //调取页面--视图  ---必须存在   test01.jsp(一会创建)
        mav.setViewName("test02");
        return mav;
    }
}
